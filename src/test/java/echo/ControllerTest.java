package echo;

import static org.junit.jupiter.api.Assertions.*;

import java.io.Console;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import echo.util.JavalinApp;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import kong.unirest.JsonNode;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import helpers.IncompatibleKeysException;
import json.*;



class ControllerTest {

    private static JavalinApp app = new JavalinApp(); // inject any dependencies you might have

    @BeforeAll
    static void init() {
        app.start(7010);
    }
    
    @AfterAll
    static void afterAll(){
        app.stop();
    }

    @Test
    void postCiclistaTest() throws JsonMappingException, JsonProcessingException, IncompatibleKeysException {
        Json<String> json = new Json<>();
        Json<String> ciclista = new Json<>();
        Json<String> meioDePagamento = new Json<>();
        Json<String> passaporte = new Json<>();

        passaporte.addField("numero", "123456789");
        passaporte.addField("validade", "2022-01-31");
        passaporte.addField("pais", "Brasil");

        meioDePagamento.addField("nomeTitular", "Joao Teste");
        meioDePagamento.addField("numero", "1234123412341324");
        meioDePagamento.addField("validade", "2022-01-31");
        meioDePagamento.addField("cvv", "123");

        ciclista.addField("nome", "jotave");
        ciclista.addField("nascimento", "2020-01-31");
        ciclista.addField("cpf", "12345678911");
        ciclista.addJson("passaporte", passaporte.response().toString());
        ciclista.addField("nacionalidade", "Brasileiro");
        ciclista.addField("email", "joao@victor.com.br");
        ciclista.addField("senha", "123456");

        json.addJson("ciclista", ciclista.response().toString());
        json.addJson("meioDePagamento", meioDePagamento.response().toString());

        HttpResponse<JsonNode> response = Unirest.post("http://localhost:7010/ciclista").header("Content-Type", "application/json").body(json.response().toString()).asJson();

        assertEquals(201, response.getStatus());
    }

    @Test
    void getCiclistaTest() throws JsonMappingException, JsonProcessingException, IncompatibleKeysException {
        HttpResponse<JsonNode> response = Unirest.get("http://localhost:7010/ciclista/51b9da9a-80ac-11ec-a8a3-0242ac120002").asJson();
    	 assertEquals(200,response.getStatus());
    }

    @Test
    void getCiclistaTestErr() throws JsonMappingException, JsonProcessingException, IncompatibleKeysException {
        HttpResponse<JsonNode> response = Unirest.get("http://localhost:7010/ciclista/404").asJson();
    	 assertEquals(404,response.getStatus());
    }

    @Test
    void putCiclistaTest() throws JsonMappingException, JsonProcessingException, IncompatibleKeysException {
        Json<String> ciclista = new Json<>();
        Json<String> passaporte = new Json<>();

        passaporte.addField("numero", "123456789");
        passaporte.addField("validade", "2022-01-31");
        passaporte.addField("pais", "Brasil");

        ciclista.addField("nome", "jotave");
        ciclista.addField("nascimento", "2020-01-31");
        ciclista.addField("cpf", "12345678911");
        ciclista.addJson("passaporte", passaporte.response().toString());
        ciclista.addField("nacionalidade", "Brasileiro");
        ciclista.addField("email", "joao@victor.com.br");

        HttpResponse<JsonNode>  response = Unirest.put("http://localhost:7010/ciclista/51b9da9a-80ac-11ec-a8a3-0242ac120002").header("Content-Type", "application/json").body(ciclista.response().toString()).asJson();

        assertEquals(200, response.getStatus());
    }

    @Test
    void putCiclistaTestErr() throws JsonMappingException, JsonProcessingException, IncompatibleKeysException {
        Json<String> ciclista = new Json<>();
        Json<String> passaporte = new Json<>();

        passaporte.addField("numero", "123456789");
        passaporte.addField("validade", "2022-01-31");
        passaporte.addField("pais", "Brasil");

        

        ciclista.addField("nome", "jotave");
        ciclista.addField("nascimento", "2020-01-31");
        ciclista.addField("cpf", "12345678911");
        ciclista.addJson("passaporte", passaporte.response().toString());
        ciclista.addField("nacionalidade", "Brasileiro");
        ciclista.addField("email", "joao@victor.com.br");

        HttpResponse<JsonNode>  response = Unirest.put("http://localhost:7010/ciclista/404").header("Content-Type", "application/json").body(ciclista.response().toString()).asJson();

        assertEquals(404, response.getStatus());
    }

    @Test
    void postAtivarTest() throws JsonMappingException, JsonProcessingException, IncompatibleKeysException {
        HttpResponse<JsonNode> response = Unirest.post("http://localhost:7010/ciclista/51b9da9a-80ac-11ec-a8a3-0242ac120002/ativar").asJson();
    	 assertEquals(200,response.getStatus());
    }

    @Test
    void postAtivarErr() throws JsonMappingException, JsonProcessingException, IncompatibleKeysException {
        HttpResponse<JsonNode> response = Unirest.post("http://localhost:7010/ciclista/404/ativar").asJson();
    	 assertEquals(404,response.getStatus());
    }

    @Test
    void getEmailExisteTest() throws JsonMappingException, JsonProcessingException, IncompatibleKeysException {
        HttpResponse<JsonNode> response = Unirest.get("http://localhost:7010/ciclista/existeEmail/joaovafigueira@gmail.com").asJson();
    	 assertEquals(200,response.getStatus());
    }

    @Test
    void getEmailExisteTestErr() throws JsonMappingException, JsonProcessingException, IncompatibleKeysException {
        HttpResponse<JsonNode> response = Unirest.get("http://localhost:7010/ciclista/existeEmail/1234").asJson();
    	 assertEquals(200,response.getStatus());
    }

    @Test
    void getFuncionariosAll() throws JsonMappingException, JsonProcessingException, IncompatibleKeysException {
        HttpResponse<JsonNode> response = Unirest.get("http://localhost:7010/funcionario").asJson();
    	 assertEquals(200,response.getStatus());
    }

    @Test
    void getFuncionarioTest() throws JsonMappingException, JsonProcessingException, IncompatibleKeysException {
        HttpResponse<JsonNode> response = Unirest.get("http://localhost:7010/funcionario/51b9da9a-80ac-11ec-a8a3-0242ac120002").asJson();
    	 assertEquals(200,response.getStatus());
    }

    @Test
    void getFuncionarioTestErr() throws JsonMappingException, JsonProcessingException, IncompatibleKeysException {
        HttpResponse<JsonNode> response = Unirest.get("http://localhost:7010/funcionario/123456").asJson();
    	 assertEquals(404,response.getStatus());
    }

    @Test
    void putFuncionarioTest() throws JsonMappingException, JsonProcessingException, IncompatibleKeysException {

        Json<String> json = new Json<>();

        json.addField("senha", "123456789");
        json.addField("email", "joao@emial.com");
        json.addField("nome", "joao");
        json.addField("idade", "0");
        json.addField("funcao", "Reparador");
        json.addField("cpf", "123456789");

        HttpResponse<JsonNode> response = Unirest.put("http://localhost:7010/funcionario/51b9da9a-80ac-11ec-a8a3-0242ac120002").header("Content-Type", "application/json").body(json.response().toString()).asJson();
    	 assertEquals(200,response.getStatus());
    }

    @Test
    void putFuncionarioTestErr() throws JsonMappingException, JsonProcessingException, IncompatibleKeysException {

        Json<String> json = new Json<>();

        json.addField("senha", "123456789");
        json.addField("email", "joao@emial.com");
        json.addField("nome", "joao");
        json.addField("idade", "0");
        json.addField("funcao", "Reparador");
        json.addField("cpf", "123456789");

        HttpResponse<JsonNode> response = Unirest.put("http://localhost:7010/funcionario/51244135").header("Content-Type", "application/json").body(json.response().toString()).asJson();
    	 assertEquals(404,response.getStatus());
    }

    @Test
    void postFuncionarioTest() throws JsonMappingException, JsonProcessingException, IncompatibleKeysException {

        Json<String> json = new Json<>();

        json.addField("senha", "123456789");
        json.addField("email", "joao@emial.com");
        json.addField("nome", "joao");
        json.addField("idade", "0");
        json.addField("funcao", "Reparador");
        json.addField("cpf", "123456789");

        HttpResponse<JsonNode> response = Unirest.post("http://localhost:7010/funcionario").header("Content-Type", "application/json").body(json.response().toString()).asJson();
    	 assertEquals(200,response.getStatus());
    }

    @Test
    void deleteFuncionarioTest() throws JsonMappingException, JsonProcessingException, IncompatibleKeysException {
        HttpResponse<JsonNode> response = Unirest.delete("http://localhost:7010/funcionario/51b9da9a-80ac-11ec-a8a3-0242ac120002").asJson();
    	 assertEquals(200,response.getStatus());
    }

    @Test
    void deleteFuncionarioTestErr() throws JsonMappingException, JsonProcessingException, IncompatibleKeysException {
        HttpResponse<JsonNode> response = Unirest.delete("http://localhost:7010/funcionario/123456").asJson();
    	 assertEquals(404,response.getStatus());
    }

    @Test
    void getCCTest() throws JsonMappingException, JsonProcessingException, IncompatibleKeysException {
        HttpResponse<JsonNode> response = Unirest.get("http://localhost:7010/cartaoDeCredito/51b9da9a-80ac-11ec-a8a3-0242ac120002").asJson();
    	 assertEquals(200,response.getStatus());
    }

    @Test
    void getCCTestErr() throws JsonMappingException, JsonProcessingException, IncompatibleKeysException {
        HttpResponse<JsonNode> response = Unirest.get("http://localhost:7010/cartaoDeCredito/123456").asJson();
    	 assertEquals(404,response.getStatus());
    }

    @Test
    void putCCTest() throws JsonMappingException, JsonProcessingException, IncompatibleKeysException {

        Json<String> json = new Json<>();

        json.addField("nomeTitular", "Joao Teste");
        json.addField("numero", "1234123412341324");
        json.addField("validade", "2022-01-31");
        json.addField("cvv", "123");

        HttpResponse<JsonNode> response = Unirest.put("http://localhost:7010/cartaoDeCredito/51b9da9a-80ac-11ec-a8a3-0242ac120002").header("Content-Type", "application/json").body(json.response().toString()).asJson();
    	 assertEquals(200,response.getStatus());
    }

    @Test
    void putCCTestErr() throws JsonMappingException, JsonProcessingException, IncompatibleKeysException {

        Json<String> json = new Json<>();

        json.addField("nomeTitular", "Joao Teste");
        json.addField("numero", "1234123412341324");
        json.addField("validade", "2022-01-31");
        json.addField("cvv", "123");

        HttpResponse<JsonNode> response = Unirest.put("http://localhost:7010/cartaoDeCredito/51244135").header("Content-Type", "application/json").body(json.response().toString()).asJson();
    	 assertEquals(404,response.getStatus());
    }

    @Test
    void alugueltest() throws JsonMappingException, JsonProcessingException, IncompatibleKeysException {

        Json<String> json = new Json<>();

        json.addField("ciclista", "51b9da9a-80ac-11ec-a8a3-0242ac120002");
        json.addField("trancaInicio", "180d95d0-621f-418f-ba44-59e96d3e48fb");


        HttpResponse<JsonNode> response = Unirest.post("http://localhost:7010/aluguel").header("Content-Type", "application/json").body(json.response().toString()).asJson();
    	 assertEquals(200,response.getStatus());
    }

    @Test
    void devolucaotest() throws JsonMappingException, JsonProcessingException, IncompatibleKeysException {

        Json<String> json = new Json<>();

        json.addField("idTranca", "0024c54b-ad07-45e9-9c83-165759e16db0");
        json.addField("idBicicleta", "0e6c3b08-1cd9-46f2-a770-3b74ab8dd71f");


        HttpResponse<JsonNode> response = Unirest.post("http://localhost:7010/devolucao").header("Content-Type", "application/json").body(json.response().toString()).asJson();
    	 assertEquals(200,response.getStatus());
    }
}

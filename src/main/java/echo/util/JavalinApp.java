package echo.util;

import io.javalin.Javalin;
import static io.javalin.apibuilder.ApiBuilder.*;
import Ciclista.CiclistaController;
import Funcionario.FuncionarioController;
import Aluguel.AluguelController;

public class JavalinApp {

    public static final String PATH = "/funcionario/:id";

    private Javalin app = 
            Javalin.create(config -> config.defaultContentType = "application/json")
                .routes(() -> {
                    path("/ciclista/:id", () -> get(CiclistaController::getCiclista));
                    path("/ciclista", ()-> post(CiclistaController::postCiclista));
                    path("/ciclista/:id", ()-> put(CiclistaController::putCiclista));
                    path("/ciclista/:id/ativar", ()-> post(CiclistaController::ativarCadastro));
                    path("/ciclista/existeEmail/:email", ()-> get(CiclistaController::existeEmail));
                    path("/funcionario", ()-> get(FuncionarioController::getAllFuncionarios));
                    path("/funcionario", ()-> post(FuncionarioController::postFuncionario));
                    path(PATH, ()-> get(FuncionarioController::getFuncionario));
                    path(PATH, ()-> put(FuncionarioController::putFuncionario));
                    path(PATH, ()-> delete(FuncionarioController::deleteFuncionario));
                    path("/cartaoDeCredito/:id", ()-> get(CiclistaController::getCC));
                    path("/cartaoDeCredito/:id", ()-> put(CiclistaController::putCC));
                    path("/aluguel", ()-> post(AluguelController::realizarAluguel));
                    path("/devolucao", ()-> post(AluguelController::devolucao));
                    });
                    


    public void start(int port) {
        this.app.start(port);
    }

    public void stop() {
        this.app.stop();
    }
}

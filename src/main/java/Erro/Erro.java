package Erro;

public class Erro {
    public String id;
    public String codigo;
    public String mensagem;

    public Erro(String id, String codigo, String mensagem){
        this.id = id;
        this.codigo = codigo;
        this.mensagem = mensagem;
    }
}

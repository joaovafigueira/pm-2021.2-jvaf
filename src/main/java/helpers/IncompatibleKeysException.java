package helpers;

public class IncompatibleKeysException extends Exception{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public IncompatibleKeysException(String message) {
		super(message);
	}
}

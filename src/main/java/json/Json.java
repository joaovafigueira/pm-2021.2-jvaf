package json;
import java.util.ArrayList;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import helpers.IncompatibleKeysException;

public class Json<T> {
	private ArrayList<String> keys;
	private ArrayList<String> values;
	private String jsonString;
	
	public Json() {
		this.keys = new ArrayList<>();
		this.values = new ArrayList<>();
	}
	
	public void Serialize(T obj) throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		this.jsonString = mapper.writeValueAsString(obj);
	}
	
	public String serializedJsonResponse(){
		return this.jsonString;
	}
	
	public void addField(String key,String value){
		if(keys.contains(key)) {
			values.set(keys.indexOf(key), value);
			return;
		}
		keys.add("\""+key+"\"");
		values.add("\""+value+"\"");
		
	}

	public void addJson(String key,String value){
		if(keys.contains(key)) {
			values.set(keys.indexOf(key), value);
			return;
		}
		keys.add("\""+key+"\"");
		values.add(value);
		
	}
	

	public JsonNode response() throws IncompatibleKeysException, JsonMappingException, JsonProcessingException {
		if(keys.size() != values.size()) {
			throw new IncompatibleKeysException("Número de chaves e valores incompátiveis");
		}
		String json = "{";
		for(int i = 0; i<values.size();i++){
			String currentKey = keys.get(i);
			String currentValue = values.get(i);
			json+= currentKey+":"+currentValue;
			if(i < values.size()-1) {
				json+=",";
			}
		}
		json += "}";
		ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readTree(json);
        
	}
}


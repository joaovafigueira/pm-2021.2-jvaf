package Aluguel;

public class Aluguel {
    protected String bicicleta;
    protected String horaInicio;
    protected String trancaFim;
    protected String horaFim;
    protected String cobranca;
    protected String ciclista;
    protected String trancaInicio;

    public Aluguel(String bicicleta, String horaInicio, String trancaFim, String horaFim, String cobranca, String ciclista, String trancaInicio){
        this.bicicleta = bicicleta;
        this.horaInicio = horaInicio;
        this.trancaFim = trancaFim;
        this.horaFim = horaFim;
        this.cobranca = cobranca;
        this.ciclista = ciclista;
        this.trancaInicio = trancaInicio;
        
    }

    public Aluguel(String bicicleta, String horaInicio, String cobranca, String ciclista, String trancaInicio){
        this.bicicleta = bicicleta;
        this.horaInicio = horaInicio;
        this.trancaFim = null;
        this.horaFim = null;
        this.cobranca = cobranca;
        this.ciclista = ciclista;
        this.trancaInicio = trancaInicio;
        
    }

    public void setBicicleta(String bicicleta) {
        this.bicicleta = bicicleta;
    }
    public void setCiclista(String ciclista) {
        this.ciclista = ciclista;
    }
    public void setCobranca(String cobranca) {
        this.cobranca = cobranca;
    }
    public void setHoraFim(String horaFim) {
        this.horaFim = horaFim;
    }
    public void setHoraInicio(String horaInicio) {
        this.horaInicio = horaInicio;
    }
    public void setTrancaFim(String trancaFim) {
        this.trancaFim = trancaFim;
    }
    public void setTrancaInicio(String trancaInicio) {
        this.trancaInicio = trancaInicio;
    }

    public String getBicicleta() {
        return bicicleta;
    }
    public String getCiclista() {
        return ciclista;
    }
    public String getCobranca() {
        return cobranca;
    }
    public String getHoraFim() {
        return horaFim;
    }
    public String getHoraInicio() {
        return horaInicio;
    }
    public String getTrancaFim() {
        return trancaFim;
    }
    public String getTrancaInicio() {
        return trancaInicio;
    }
}

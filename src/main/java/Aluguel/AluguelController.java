package Aluguel;

import io.javalin.http.Context;
import java.time.LocalDateTime;

import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import kong.unirest.JsonNode;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import helpers.IncompatibleKeysException;
import json.*;
import Erro.Erro;
import Ciclista.Ciclista;
import Ciclista.CiclistaDAO;

public class AluguelController  {

    AluguelController(){}

    public static final String UUID = "51b9da9a-80ac-11ec-a8a3-0242ac120002";

    public static void realizarAluguel(Context ctx) throws JsonMappingException, JsonProcessingException, IncompatibleKeysException{
        AluguelForm aluguelform = ctx.bodyAsClass(AluguelForm.class);

        HttpResponse<GetTrancaResponse> resTotem = Unirest.get("https://pm-2021-2-va-de-bicicleta-test.herokuapp.com/tranca/"+aluguelform.trancaInicio).header("Content-Type", "application/json").asObject(GetTrancaResponse.class);

        if(resTotem.getStatus() != 200){
            ctx.status(resTotem.getStatus());
            ctx.json(new Erro(aluguelform.trancaInicio,"Status: "+resTotem.getStatus(),"Tranca não encontrada"));
        }

        GetTrancaResponse totem = resTotem.getBody();

        String bicicleta = totem.bicicleta;

        Json<String> json = new Json<>();

        json.addField("valor", "1");
        json.addField("ciclista", aluguelform.ciclista);
        
        HttpResponse<PostCobrancaResponse> resCobranca = Unirest.post("https://va-de-bicicleta.herokuapp.com/cobranca").header("Content-Type", "application/json").body(json.response().toString()).asObject(PostCobrancaResponse.class);

        if(resCobranca.getStatus() != 200){
            ctx.status(resCobranca.getStatus());
            ctx.json(new Erro("",""+resCobranca.getStatus(),"Cobrança não realizada"));
        }

        PostCobrancaResponse cobranca = resCobranca.getBody();

        String cobrancaId = cobranca.id;
        String horaInicio = LocalDateTime.now().toString();

        HttpResponse<GetTrancaResponse> resTranca2 = Unirest.post("https://pm-2021-2-va-de-bicicleta-test.herokuapp.com/tranca/"+aluguelform.trancaInicio+"/status/DESTRANCAR").header("Content-Type", "application/json").asObject(GetTrancaResponse.class);

        if(resTranca2.getStatus() != 200){
            ctx.status(resTranca2.getStatus());
            ctx.json(new Erro(aluguelform.trancaInicio,"Status: "+resTranca2.getStatus(),"Tranca não encontrada"));
        }

        HttpResponse<JsonNode> resBike = Unirest.post("https://pm-2021-2-va-de-bicicleta-test.herokuapp.com/Bicicleta/"+bicicleta+"/status/EM_USO").header("Content-Type", "application/json").asJson();

        if(resBike.getStatus() != 200){
            ctx.status(resBike.getStatus());
            ctx.json(new Erro("","Status: "+resBike.getStatus(),"Bicicleta não encontrada"));
        }

        Json<String> json2 = new Json<>();

        CiclistaDAO listaCiclista = new CiclistaDAO();

        Ciclista ciclista = listaCiclista.getCiclistaById(aluguelform.ciclista);

        json2.addField("email", ciclista.getEmail() );
        json2.addField("mensagem", "Aluguel da Bicicleta: "+bicicleta+" Realizado");
        
        HttpResponse<JsonNode> resEmail = Unirest.post("https://va-de-bicicleta.herokuapp.com/enviarEmail").header("Content-Type", "application/json").body(json2.response().toString()).asJson();
      
        Aluguel aluguel = new Aluguel(bicicleta, horaInicio, cobrancaId, aluguelform.ciclista, aluguelform.trancaInicio);

        ctx.status(200);
        ctx.json(aluguel);

    }

    public static void devolucao(Context ctx) throws JsonMappingException, JsonProcessingException, IncompatibleKeysException{

        DevolucaoForm devForm = ctx.bodyAsClass(DevolucaoForm.class);

        String cobranca = UUID;
        String horaFim = LocalDateTime.now().toString();

        AluguelDAO dao = new AluguelDAO();

        Aluguel aluguel = dao.getAluguelByCobranca(cobranca);

        HttpResponse<GetTrancaResponse> resTranca2 = Unirest.post("https://pm-2021-2-va-de-bicicleta-test.herokuapp.com/tranca/"+aluguel.trancaInicio+"/status/TRANCAR").header("Content-Type", "application/json").asObject(GetTrancaResponse.class);

        if(resTranca2.getStatus() != 200){
            ctx.status(resTranca2.getStatus());
            ctx.json(new Erro(aluguel.trancaInicio,"Status: "+resTranca2.getStatus(),"Tranca não encontrada"));
        }

        HttpResponse<JsonNode> resBike = Unirest.post("https://pm-2021-2-va-de-bicicleta-test.herokuapp.com/Bicicleta/"+aluguel.bicicleta+"/status/DISPONIVEL").header("Content-Type", "application/json").asJson();

        if(resBike.getStatus() != 200){
            ctx.status(resBike.getStatus());
            ctx.json(new Erro("","Status: "+resBike.getStatus(),"Bicicleta não encontrada"));
        }

        CiclistaDAO listaCiclista = new CiclistaDAO();

        Ciclista ciclista = listaCiclista.getCiclistaById(aluguel.ciclista);

        Json<String> json2 = new Json<>();

        json2.addField("email", ciclista.getEmail() );
        json2.addField("mensagem", "Devolução da Bicicleta: "+aluguel.bicicleta+" Realizada");
        
        HttpResponse<JsonNode> resEmail = Unirest.post("https://va-de-bicicleta.herokuapp.com/enviarEmail").header("Content-Type", "application/json").body(json2.response().toString()).asJson();

        aluguel.horaFim = horaFim;
        aluguel.trancaFim = devForm.idTranca;

        ctx.status(200);
        ctx.json(aluguel);
        
    }
    
}

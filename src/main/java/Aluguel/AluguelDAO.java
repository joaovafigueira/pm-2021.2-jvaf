package Aluguel;

import java.util.ArrayList;  
import java.time.LocalDateTime;

public class AluguelDAO {

    public ArrayList<Aluguel> alugueis = new ArrayList<Aluguel>();

    public AluguelDAO(){

        String uuid = "51b9da9a-80ac-11ec-a8a3-0242ac120002";

        this.alugueis.add(
            new Aluguel(uuid, LocalDateTime.now().toString(), uuid,uuid,uuid
        ));
    }

    public Aluguel getAluguelByCobranca(String cobranca){
        try{
            for (int i = 0; i<this.alugueis.size(); i++){
                if(alugueis.get(i).getCobranca() == cobranca){
                    return alugueis.get(i);
                }
            }
        }catch(NullPointerException e){
            return null;
        }
        return null;
    }

    public void addAluguel(Aluguel aluguel){
        this.alugueis.add(aluguel);
    }

    
}

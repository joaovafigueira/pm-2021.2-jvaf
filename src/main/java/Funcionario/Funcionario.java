package Funcionario;

import java.util.*;

public class Funcionario {

    protected String id;
    protected String matricula;
    protected String email;
    protected String nome;
    protected int idade;
    protected String funcao;
    protected String cpf;

    public Funcionario(String email, String nome, int idade, String funcao, String cpf){
        Random rand = new Random();

        int matriculaInt = rand.nextInt(1000000);
        matriculaInt += 1000000*(rand.nextInt(6)+1);

        UUID uuid = UUID.randomUUID();
        this.id = uuid.toString();

        this.matricula = Integer.toString(matriculaInt); 
        this.email = email;
        this.nome = nome;
        this.idade = idade;
        this.funcao = funcao;
        this.cpf = cpf;

    }

    public Funcionario(String id,String matricula, String email, String nome, int idade, String funcao, String cpf){
        this.id = id;
        this.matricula = matricula; 
        this.email = email;
        this.nome = nome;
        this.idade = idade;
        this.funcao = funcao;
        this.cpf = cpf;

    }

    public String getCpf() {
        return cpf;
    }
    public String getEmail() {
        return email;
    }
    public String getFuncao() {
        return funcao;
    }
    public int getIdade() {
        return idade;
    }
    public String getMatricula() {
        return matricula;
    }
    public String getNome() {
        return nome;
    }
    public String getId() {
        return id;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public void setFuncao(String funcao) {
        this.funcao = funcao;
    }
    public void setId(String id) {
        this.id = id;
    }
    public void setIdade(int idade) {
        this.idade = idade;
    }
    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
}

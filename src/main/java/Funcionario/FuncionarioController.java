package Funcionario;

import io.javalin.http.Context;
import Erro.Erro;

import java.util.Objects;

public class FuncionarioController {
    
    FuncionarioController(){}

    public static void getFuncionario (Context ctx){

        String idFuncionario = Objects.requireNonNull(ctx.pathParam("id"));

        FuncionarioDAO listaFuncionario = new FuncionarioDAO();

        Funcionario funcionario = listaFuncionario.getFuncionarioById(idFuncionario);

        if(funcionario != null){
            ctx.status(200);
            ctx.json(funcionario);
        } else {
            ctx.json(new Erro(idFuncionario,"404","Funcionario não localizado"));
            ctx.status(404);
        }
    }

    public static void putFuncionario(Context ctx){
        String idFuncionario = Objects.requireNonNull(ctx.pathParam("id"));

        FuncionarioDAO listaFuncionarios = new FuncionarioDAO();

        Funcionario funcionario = listaFuncionarios.getFuncionarioById(idFuncionario);

        if(funcionario == null){
            ctx.json(new Erro(idFuncionario,"404","Funcionario não existe"));
            ctx.status(404);
            return;
        }

        Funcionario funcionarioPut = ctx.bodyAsClass(FuncionarioForm.class).desserialize();

        funcionario.setNome(funcionarioPut.nome);
        funcionario.setIdade(funcionarioPut.idade);
        funcionario.setCpf(funcionarioPut.cpf);
        funcionario.setEmail(funcionarioPut.email);
        funcionario.setFuncao(funcionarioPut.funcao);

        ctx.status(200);
        ctx.json(funcionario);
    }

    public static void getAllFuncionarios(Context ctx){
        FuncionarioDAO listaFuncionarios = new FuncionarioDAO();
        ctx.status(200);
        ctx.json(listaFuncionarios.funcionarios.toArray());
    }

    public static void deleteFuncionario(Context ctx){
        String idFuncionario = Objects.requireNonNull(ctx.pathParam("id"));

        FuncionarioDAO listaFuncionario = new FuncionarioDAO();

        Funcionario funcionario = listaFuncionario.getFuncionarioById(idFuncionario);

        if(funcionario != null){
            listaFuncionario.deleteFuncionario(idFuncionario);
            ctx.status(200);
            ctx.html("Dados Removidos");
        } else {
            ctx.json(new Erro(idFuncionario,"404","Funcionario não encontrado"));
            ctx.status(404);
        }
    }

    public static void postFuncionario(Context ctx){

        Funcionario funcionario = ctx.bodyAsClass(FuncionarioForm.class).desserialize();
        
        FuncionarioDAO listaFuncionario = new FuncionarioDAO();

        listaFuncionario.addFuncionario(funcionario);

        ctx.status(200);
        ctx.json(funcionario);

    }

}

package Funcionario;

public class FuncionarioForm {
    public String senha;
    public String email;
    public String nome;
    public int idade;
    public String funcao;
    public String cpf;

    public FuncionarioForm(){
      //Body As Class
    }

    public Funcionario desserialize(){
		return new Funcionario(this.email, this.nome, this.idade, this.funcao, this.cpf);
	}
}

package Funcionario;

import java.util.ArrayList;  

public class FuncionarioDAO {

    public ArrayList<Funcionario> funcionarios = new ArrayList<Funcionario>();
    
    FuncionarioDAO(){
        this.funcionarios.add(
            new Funcionario("51b9da9a-80ac-11ec-a8a3-0242ac120002","20181210019", "joaovafigueira@gmail.com","João Victor", 22, "Reparador", "000.000.000-00")
        );
    }

    public Funcionario getFuncionarioById(String id){
        try{
            for (int i = 0; i<this.funcionarios.size(); i++){
                if(funcionarios.get(i).getId().equals(id)){
                    return funcionarios.get(i);
                }
            }
        }catch(NullPointerException e){
            return null;
        }
        return null;
    }

    public void addFuncionario(Funcionario funcionario){
        this.funcionarios.add(funcionario);
    }

    public void deleteFuncionario(String id){

        try{
            for (int i = 0; i<this.funcionarios.size(); i++){
                if(funcionarios.get(i).getId().equals(id)){
                    funcionarios.remove(i);
                }
            }
        }catch (NullPointerException e){
            return;
        }

    }

}

package Ciclista;

import java.util.ArrayList;  

public class CiclistaDAO {
    
    public CiclistaDAO(){

        String dataNascJv = "1999-10-01";
        String dataValJv = "2030-10-01";
        Passaporte passaporte = new Passaporte("BR123456", "Brasil", dataValJv);

        this.ciclistas.add(
            new Ciclista_Cartao(
                new Ciclista("51b9da9a-80ac-11ec-a8a3-0242ac120002" ,"João Victor Antunes Figueira", dataNascJv, "000.000.000-00", "Brasileiro", "joaovafigueira@gmail.com",passaporte),
                new CartaoDeCredito("João Teste","0000.0000.0000.0001", "2030-01-10", "123")
        ));
    }

    public ArrayList<Ciclista_Cartao> ciclistas = new ArrayList<Ciclista_Cartao>();

    public Ciclista_Cartao getCiclistaCCByCiclista(Ciclista ciclista){
        try{
            for (int i = 0; i<this.ciclistas.size(); i++){
                if(ciclistas.get(i).getCiclista() == this.getCiclistaById(ciclista.getId())){
                    return ciclistas.get(i);
                }
            }
        }catch(NullPointerException e){
            return null;
        }
        return null;
    }
    
    public Ciclista getCiclistaById(String id){
        try{
            for (int i = 0; i<this.ciclistas.size(); i++){
                if(ciclistas.get(i).getCiclista().getId().equals(id)){
                    return ciclistas.get(i).getCiclista();
                }
            }
        }catch(NullPointerException e){
            return null;
        }
        return null;
    }

    public Ciclista getCiclistaByEmail(String email){
        try{
            for (int i = 0; i<this.ciclistas.size(); i++){
                if(ciclistas.get(i).getCiclista().getEmail().equals(email)){
                    return ciclistas.get(i).getCiclista();
                }
            }
        }catch(NullPointerException e){
            return null;
        }
        return null;
    }

    public CartaoDeCredito getCCById(String id){
        try{
            for (int i = 0; i<this.ciclistas.size(); i++){
                if(ciclistas.get(i).getCiclista().getId().equals(id)){
                    return ciclistas.get(i).getCc();
                }
            }
        }catch(NullPointerException e){
            return null;
        }
        return null;
    }

    public void addCiclista(Ciclista_Cartao ciclista){
        this.ciclistas.add(ciclista);
    }

}

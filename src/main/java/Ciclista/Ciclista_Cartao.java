package Ciclista;

public class Ciclista_Cartao {
    private Ciclista ciclista;
    private CartaoDeCredito cc;

    public Ciclista_Cartao(Ciclista ciclista, CartaoDeCredito cc){
        this.ciclista = ciclista;
        this.cc = cc;
    }

    // Getter
    public CartaoDeCredito getCc() {
        return cc;
    }
    public Ciclista getCiclista() {
        return ciclista;
    }
    // Setter
    public void setCc(CartaoDeCredito cc) {
        this.cc = cc;
    }
    public void setCiclista(Ciclista ciclista) {
        this.ciclista = ciclista;
    }

}

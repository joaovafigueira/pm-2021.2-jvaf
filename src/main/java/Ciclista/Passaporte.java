package Ciclista;

public class Passaporte {

    public Passaporte(){
        // Construtor vazio msm
    }

    public Passaporte(String numero, String validade, String pais){

        this.numero = numero;
        this.validade = validade;
        this.pais = pais;
    }
    
    private String numero;
    private String validade;
    private String pais;

    // Getters

    public String getNumero() {
        return numero;
    }
    public String getPais() {
        return pais;
    }
    public String getValidade() {
        return validade;
    }

    // Setters

    public void setNumero(String numero) {
        this.numero = numero;
    }
    public void setPais(String pais) {
        this.pais = pais;
    }
    public void setValidade(String validade) {
        this.validade = validade;
    }
    
}    
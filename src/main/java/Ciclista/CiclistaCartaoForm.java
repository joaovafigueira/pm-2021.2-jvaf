package Ciclista;

public class CiclistaCartaoForm {
    public Ciclista ciclista;
    public CartaoDeCredito meioDePagamento;

    public CiclistaCartaoForm(){
        //Precisa Existir para o Body as Class
    }

    public Ciclista_Cartao desserialize(){
    
        return new Ciclista_Cartao(this.ciclista, this.meioDePagamento);

    }

}

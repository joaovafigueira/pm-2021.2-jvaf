package Ciclista;

import io.javalin.http.Context;
import Erro.Erro;

import java.util.Objects;

public class CiclistaController {

    private CiclistaController(){
        //Do Something
    }

    public static void getCiclista (Context ctx){

        String idCiclista = Objects.requireNonNull(ctx.pathParam("id"));

        CiclistaDAO listaCiclista = new CiclistaDAO();

        Ciclista ciclista = listaCiclista.getCiclistaById(idCiclista);

        if(ciclista != null){
            ctx.status(200);
            ctx.json(ciclista);
        } else {
            ctx.json(new Erro(idCiclista,"404","Ciclista não localizado"));
            ctx.status(404);
        }
    }

    public static void getCC (Context ctx){
        String idCiclista = Objects.requireNonNull(ctx.pathParam("id"));

        CiclistaDAO listaCiclista = new CiclistaDAO();

        CartaoDeCredito cc = listaCiclista.getCCById(idCiclista);

        if(cc != null){
            ctx.status(200);
            ctx.json(cc);
        } else {
            ctx.json(new Erro(idCiclista,"404","Cartão de Crédito não encontrado"));
            ctx.status(404);
        }
    }

    public static void putCC(Context ctx){
        String idCiclista = Objects.requireNonNull(ctx.pathParam("id"));

        CiclistaDAO listaCiclista = new CiclistaDAO();

        CartaoDeCredito cc = listaCiclista.getCCById(idCiclista);

        if(cc == null){
            ctx.json(new Erro(idCiclista,"404","Cartão de Crédito não encontrado"));
            ctx.status(404);
            return;
        }

        CartaoDeCredito ccPut = ctx.bodyAsClass(CartaoDeCreditoForm.class).desserialize();

        cc.setCvv(ccPut.getCvv());
        cc.setNomeTitular(ccPut.getNomeTitular());
        cc.setNumero(ccPut.getNumero());
        cc.setValidade(ccPut.getValidade());

        ctx.status(200);
        ctx.html("Dados Atualizados");
    }

    public static void putCiclista(Context ctx){
        String idCiclista = Objects.requireNonNull(ctx.pathParam("id"));

        CiclistaDAO listaCiclista = new CiclistaDAO();

        Ciclista ciclista = listaCiclista.getCiclistaById(idCiclista);

        if(ciclista == null){
            ctx.json(new Erro(idCiclista,"404","Ciclista não encontrado"));
            ctx.status(404);
            return;
        }

        CiclistaForm ciclistaPut = ctx.bodyAsClass(CiclistaForm.class);

        ciclista.setNome(ciclistaPut.nome);
        ciclista.setNascimento(ciclistaPut.nascimento);
        ciclista.setCpf(ciclistaPut.cpf);
        ciclista.setEmail(ciclistaPut.email);
        ciclista.setPassaporte(ciclistaPut.passaporte);

        ctx.status(200);
        ctx.html("Dados Atualizados");
    }

    public static void postCiclista(Context ctx){

        Ciclista_Cartao ciclista = ctx.bodyAsClass(CiclistaCartaoForm.class).desserialize();
        
        CiclistaDAO listaCiclista = new CiclistaDAO();

        listaCiclista.addCiclista(ciclista);

        ctx.status(201);
        ctx.html("Ciclista Cadastrado");

    }

    public static void ativarCadastro (Context ctx){

        String idCiclista = Objects.requireNonNull(ctx.pathParam("id"));

        CiclistaDAO listaCiclista = new CiclistaDAO();

        Ciclista ciclista = listaCiclista.getCiclistaById(idCiclista);

        if(ciclista != null){
            ciclista.ativarCiclista();
            ctx.status(200);
            ctx.json(ciclista);
        }else{
            ctx.json(new Erro(idCiclista,"404","Ciclista não existe"));
            ctx.status(404);
        }
    }

    public static void existeEmail (Context ctx){

        String emailCiclista = Objects.requireNonNull(ctx.pathParam("email"));

        CiclistaDAO listaCiclista = new CiclistaDAO();

        Ciclista ciclista = listaCiclista.getCiclistaByEmail(emailCiclista);

        if(ciclista != null){
            ctx.status(200);
            ctx.html("true");  
        }else{
            ctx.status(200);
            ctx.html("false"); 
        } 
            
    }
    
}

package Ciclista;

import java.util.*;

public class Ciclista{

    public enum Status {
        AGUARDANDO_CONFIRMACAO, ATIVO, INATIVO;
        }

    public Ciclista(){
        UUID uuid = UUID.randomUUID();
        this.id = uuid.toString();
        this.status = Status.AGUARDANDO_CONFIRMACAO.toString();
    }

    public Ciclista(String id, String name, String nasc, String cpf, String nacionaldade, String email, Passaporte passaporte){

        this.id = id;
        this.nome = name;
        this.nascimento = nasc;
        this.cpf = cpf;
        this.email = email;
        this.passaporte = passaporte;
        this.status = Status.ATIVO.toString();

        this.setNacionalidade(nacionaldade);
    }
    
    private String id;
    protected String nome;
    protected String nascimento;
    protected String cpf;
    protected String email;
    protected Passaporte passaporte;
    protected String status;
    protected String nacionalidade;
    protected String senha;

    //Getters

    public String getCpf() {
        return cpf;
    }
    public String getEmail() {
        return email;
    }
    public String getId() {
        return id;
    }
    public String getNacionalidade() {
        return nacionalidade;
    }
    public String getNascimento() {
        return nascimento;
    }
    public String getNome() {
        return nome;
    }
    public Passaporte getPassaporte() {
        return passaporte;
    }
    public String getStatus() {
        return status;
    }

    //Setters

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public void setId(String id) {
        this.id = id;
    }
    public void setNascimento(String nascimento) {
        this.nascimento = nascimento;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    public void setPassaporte(Passaporte passaporte) {
        this.passaporte = passaporte;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public void setNacionalidade(String nacionalidade) {
        this.nacionalidade = nacionalidade;
    }
    public void setSenha(String senha) {
        this.senha = senha;
    }

    //Handlers

    public boolean ativarCiclista(){
        this.setStatus(Status.ATIVO.toString()); 
        return true;
    }

    
}
